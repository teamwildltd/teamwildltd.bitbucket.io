(function ($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 70)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });
  
  /* Mobile Menu
  ================================================== */
  $('#site-navigation').mmenu({
    extensions: ['theme-dark', 'border-full', 'pagedim-black'],
      'offCanvas': {
        //moveBackground: true,
        pageSelector: "#page-wrap",
        zposition: 'next'
      }
    },{
      clone: true
    }
  );

  var api = $("#mm-site-navigation").data( "mmenu" );

  // Fixed elements
  api.bind( "open:before", function( $panel ) {
    var fromTop = $(document).scrollTop();
    $(".main-menu-area.fixed-top").css("top", fromTop +"px");
  });
  api.bind( "close:finish", function( $panel ) {
    $(".main-menu-area.fixed-top").css("top", 0 +"px");
  });
  
  /* Footer Collapse 
  ================================================== */
  
  $('#acc').find('.acc-toggle').click(function(){

    //Expand or collapse this panel
    $(this).next(".footer-col-content").slideToggle('fast');

    //Hide the other panels
    $(".footer-col-content").not($(this).next()).slideUp('fast');

  });

  /* Main nav 
  ================================================== */

  $('.main-menu-area #site-navigation ul.sf-menu').superfish({
    popUpSelector: 'ul.sub-menu,.sf-mega', 	// within menu context
    delay: 200,                	// one second delay on mouseout
    speed: 0,               		// faster \ speed
    speedOut: 200,             		// speed of the closing animation
    animation: { opacity: 'show' },		// animation out
    animationOut: { opacity: 'hide' },		// adnimation in
    cssArrows: true,              		// set to false
    autoArrows: true,                    // disable generation of arrow mark-up
    disableHI: true,
  });

  /* Scroll to top button appear
  ================================================== */
  $(document).scroll(function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  /* Initiate Parallax 
  ================================================== */
  $.Scrollax();
  
  // Collapse Navbar
  var navbarCollapse = function () {
    var window_top = $(window).scrollTop();
    var header_height = $("#page-header").outerHeight();
    var header_top = $("#page-header").offset().top + header_height - 44;

    if (window_top > header_top) {
      $(".main-menu-area").addClass("fixed-top");
    } else {
      $(".main-menu-area").removeClass("fixed-top");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);



  // Responsive typography
  $(".display-4").fitText(1, { minFontSize: '32px', maxFontSize: '56px' });

  // Responsive videos
  $("#page-wrap").fitVids();

  // Floating label headings for the contact form
  $(function () {
    $("body").on("input propertychange", ".floating-label-form-group", function (e) {
      $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function () {
      $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function () {
      $(this).removeClass("floating-label-form-group-with-focus");
    });
  });

})(jQuery); // End of use strict